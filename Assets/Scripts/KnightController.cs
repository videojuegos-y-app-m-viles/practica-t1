using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnightController : MonoBehaviour
{

    private SpriteRenderer  sr;
    private Rigidbody2D rb;
    private Animator animator;

    public float velocityX = 4;
    public float forceJump = 2500;

    private const int ANIMATION_IDLE = 0;
    private const int ANIMATION_WALK = 2;
    private const int ANIMATION_RUN = 3;
    private const int ANIMATION_ATACK = 4;
    private const int ANIMATION_JUMP = 5;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Inicializando juego");
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer >();
    }
    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector2(0, rb.velocity.y);
        ChangeAnimation(ANIMATION_IDLE); 
       //derecha
        if (Input.GetKey(KeyCode.RightArrow))
        {
            Debug.Log("Debería avanzar");
         
            rb.velocity = Vector2.right * velocityX;
            sr.flipX = false;
            ChangeAnimation(ANIMATION_WALK);
            //correr
            if(Input.GetKey(KeyCode.X)){
                rb.velocity = Vector2.right * velocityX * 2;
                ChangeAnimation(ANIMATION_RUN);
            }
            
        }
        //izquierda
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb.velocity = Vector2.right * -velocityX;
            sr.flipX = true;
            ChangeAnimation(ANIMATION_WALK);
            //correr
            if(Input.GetKey(KeyCode.X)){
                rb.velocity = Vector2.right * -(velocityX * 2);
                ChangeAnimation(ANIMATION_RUN);
            }
        }
        //saltar
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce( Vector2.up * forceJump, ForceMode2D.Impulse);
            ChangeAnimation(ANIMATION_JUMP);
        }
        //atacar
        if (Input.GetKey(KeyCode.Z))
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
            ChangeAnimation(ANIMATION_ATACK);
        }
    }
    private void ChangeAnimation(int animation){
        animator.SetInteger("estado",animation);
    }
}
